import javax.smartcardio.CardChannel;

public class Journee{

    private int nombreSpectateursMatin;
    private int nombreSpectateursSoir;
    private Style styleMatin;
    private Style styleSoir;

    public Journee(int nbSpecAM, Style styleAM, int nbSpecSoir, Style styleSoir){
        this.nombreSpectateursMatin = nbSpecAM;
        this.nombreSpectateursSoir = nbSpecSoir;
        this.styleMatin = styleAM;
        this.styleSoir = styleSoir;
    }

    public int getNbSpectateursAM() {
        return this.nombreSpectateursMatin;
    }

    public int getNbSpectateursSoir() {
        return this.nombreSpectateursSoir;
    }
    
    public Style getStyleAM() {
        return this.styleMatin;
    }

    public Style getStyleSoir() {
        return this.styleSoir;
    }

    public float score(Auteur a){
        return this.getNbSpectateursAM()*a.qualiStyle(this.styleMatin) + this.getNbSpectateursSoir()*a.qualiStyle(this.styleSoir);
    }
    
}