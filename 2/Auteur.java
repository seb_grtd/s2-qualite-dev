public class Auteur{
    
    private String nom;

    private String citationComedie;
    private String citationTragedie;
    private String citationDrame;

    private float qualiteComedie;
    private float qualiteTragedie;
    private float qualiteDrame;

    public Auteur(float qualiteTragedie, String citationTragedie, float qualiteComedie, String citationComedie, float qualiteDrame, String citationDrame, String nom){
        this.qualiteTragedie = qualiteTragedie;
        this.citationTragedie = citationTragedie;

        this.qualiteComedie = qualiteComedie;
        this.citationComedie = citationComedie;

        this.qualiteDrame = qualiteDrame;
        this.citationDrame = citationDrame;

        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }
    
    public float getQualiteComedie() {
        return qualiteComedie;
    }
    
    public float getQualiteDrame() {
        return qualiteDrame;
    }

    public String getCitationTragedie() {
        return citationTragedie;
    }

    public String getCitationComedie() {
        return citationComedie;
    }

    public String getCitationDrame() {
        return citationDrame;
    }
    public float getQualiteTragedie() {
        return qualiteTragedie;
    }

    @Override
    public String toString() {
        return "L'honorable "+this.nom;
    }

    public Style pointFort(){
        if (qualiteComedie > qualiteDrame && qualiteComedie > qualiteTragedie)
            return Style.COMÉDIE;
        else if (qualiteDrame > qualiteComedie && qualiteDrame > qualiteTragedie)
            return Style.DRAME;
        else
            return Style.TRAGÉDIE;
    }
    
    public float qualiStyle(Style s){
        if (s == Style.COMÉDIE)
            return this.getQualiteComedie();
        else if (s == Style.TRAGÉDIE)
            return this.getQualiteTragedie();
        else
            return this.getQualiteDrame();
    }

    public String citationStyle(Style s){
        if (s == Style.COMÉDIE)
            return this.getCitationComedie();
        else if (s == Style.TRAGÉDIE)
            return this.getCitationTragedie();
        else
            return this.getCitationDrame();
    }

}