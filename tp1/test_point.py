from geom import *
def test_creation():
    p = point(22 ,7)
    assert 22 == getx(p)
    assert 7 == gety(p)

def test_rectangle():
    r = Rectangle(2,4,4,5)
    assert getRectX(r) == 2
    assert getRectY(r) == 4
    assert getwidth(r) == 4
    assert getheight(r) == 5
    assert getcenter(r) == [(2+4)//2, (4+5)//2]
    assert getsurface(r) == 4*5
    set_center(r, (2,5))
    assert getRectX(r) == 0
    assert getRectY(r) == 5



    # x+4//2 == 2
    # x+4 = 4
    # x = 0

    # y+5//2 = 5
    # y+5 = 10
    # y = 5