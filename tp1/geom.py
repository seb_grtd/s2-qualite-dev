def point(x,y):
    return {'x': x, 'y': y}
def getx(p):
    return p['x']
def gety(p):
    return p['y']

def Rectangle(x,y,w,h):
    return [x,y,w,h]

def getRectX(rectangle):
    return rectangle[0]

def getRectY(rectangle):
    return rectangle[1]

def getwidth(rectangle):
    return rectangle[2]

def getheight(rectangle):
    return rectangle[3]

def getcenter(rectangle):
    return [((getRectX(rectangle)+getwidth(rectangle))//2), ((getRectY(rectangle)+getheight(rectangle))//2)]

def getsurface(rectangle):
    return getwidth(rectangle)*getheight(rectangle)

def set_center(rectangle, p):
    rectangle[0] = (p[0]*2-getwidth(rectangle))
    rectangle[1] = (p[1]*2-getheight(rectangle))
    